# Computación Gráfica

Prácticas realizadas para la asignatura de Computación Gráfica.

## Proyectos

* [Algoritmo de Liang-Barsky](liang_barsky_algorithm/)

  Implementación del algoritmo de recorte de líneas.

* [PolygonArt a OpenGL](opengl_imagenes/)

  Proyecto donde a partir de una imagen de PolygonArt, con ayuda
  del software Inkscape, se pueden marcar los polígonos y procesar
  por un script para obtener la imagen implementada con poligonos
  en OpenGL.


* [Proyecto Final](proyecto_final/)

  Creación de un entorno en 3D, con animaciónes básicas y texturas.
  Totalmente implementado en OpenGL.


## Licencia

[GPLv3](COPYING)


